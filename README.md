В этом приложение вы можете посмотреть текущий курс основных валют и конвертировать любую сумму в рублях в любую валюту из списка.

Описание основного функционала:
При открытии приложения вы увидите все доступные валюты и их курс. Курс предоставляет цену 1-ой единицы валюты в рублях. Для каждой валюты представлен их код, состоящий из 3 заглавных букв латинского алфавита. Ниже можно увидеть расшифровку на русском языке.

На верхней панели приложения представлены 2 кнопки. Кнопка, которая находится левее позволит вам конвертировать сумму в рублях в любую валюту из списка. Для этого нужно нажать на кнопку и перед вами появиться окно для конвертации.
В этом окне вы можете ввести сумму конвертации в рублях и код валюты, в которую нужно конвертировать. Код состоит из 3 заглавных латинских букв. Далее нужно нажать кнопку "Конвертировать" и вы увидите итог конвертации. Чтобы выйти из этого окна, нажмите на кнопку "Выйти".

Также, рядом с кнопкой для конвертации валют находится кнопка для обновления нашего списка валют. Чтобы увидеть самую актуальную информацию, вам нужно лишь нажать на эту кнопку.
