package com.example.currencies;

public class Currency {
    private String id;
    private String charCode;
    private String name;
    private double value;

    public Currency(String id, String charCode, String name, double value) {
        this.id = id;
        this.charCode = charCode;
        this.name = name;
        this.value = value;
    }

    public static final String[] charCodes = {"AUD", "AZN", "GBP", "AMD", "BYN", "BGN", "BRL",
    "HUF", "HKD", "DKK", "USD", "EUR", "INR", "KZT", "CAD", "KGS", "CNY", "MDL", "NOK", "PLN", "RON",
    "XDR", "SGD", "TJS", "TRY", "TMT", "UZS", "UAH", "CZK", "SEK", "CHF", "ZAR", "KRW", "JPY"};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharCode() {
        return charCode;
    }

    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
