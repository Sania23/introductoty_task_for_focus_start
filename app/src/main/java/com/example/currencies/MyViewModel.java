package com.example.currencies;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyViewModel extends ViewModel {
    MutableLiveData<List<Currency>> data;

    public LiveData<List<Currency>> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
            loadData();
        }
        return data;
    }

    public void loadData() {
        DownloadInfoAboutCurrencies task = new DownloadInfoAboutCurrencies();
        List<Currency> currencies = new ArrayList<>();
        task.execute("https://www.cbr-xml-daily.ru/daily_json.js");
        try {
            currencies = task.get();

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        data.postValue(currencies);
    }

    private static class DownloadInfoAboutCurrencies extends AsyncTask<String, Void, List<Currency>> {
        @Override
        protected List<Currency> doInBackground(String... strings) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();
            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String line = reader.readLine();
                while (line != null) {
                    result.append(line);
                    line = reader.readLine();
                }
                List<Currency> valutes = new ArrayList<>();
                JSONObject obj = new JSONObject(result.toString());
                JSONObject currencies = obj.getJSONObject("Valute");
                for (int i = 0; i < Currency.charCodes.length; ++i) {
                    JSONObject currency = currencies.getJSONObject(Currency.charCodes[i]);
                    String id = currency.getString("ID");
                    String charCode = currency.getString("CharCode");
                    String name = currency.getString("Name");
                    double value = currency.getDouble("Value");
                    Currency c = new Currency(id, charCode, name, value);
                    valutes.add(c);
                }
                return valutes;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }
    }
}
