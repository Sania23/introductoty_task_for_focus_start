package com.example.currencies;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    LiveData<List<Currency>> currencies;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyViewModel model = ViewModelProviders.of(this).get(MyViewModel.class);
        currencies = model.getData();
        currencies.observe(this, new Observer<List<Currency>>() {
            @Override
            public void onChanged(@Nullable List<Currency> c) {
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.currencies_recycler);
                CurrencyAdapter adapter = new CurrencyAdapter(c);
                recyclerView.setAdapter(adapter);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_convert:
                convertValute();
                return true;
            case R.id.action_refresh:
                refreshData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void convertValute() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(R.layout.converting_layout);
        builder.setPositiveButton("Конвертировать", null);
        builder.setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = view.getRootView();
                EditText enteredAmountEditText = (EditText) v.findViewById(R.id.entered_amount);
                EditText valuteOfConvertingEditText = (EditText) v.findViewById(R.id.valute_for_converting);
                TextView resultOfConvertingTextView = (TextView) v.findViewById(R.id.result_of_converting);
                double enteredAmount = Double.parseDouble(enteredAmountEditText.getText().toString());
                String valuteOfConverting = valuteOfConvertingEditText.getText().toString();
                if (isCorrectValute(valuteOfConverting)) {
                    Currency c = getCurrencyByValute(valuteOfConverting, currencies.getValue());
                    if (c == null) {
                        Toast.makeText(getApplicationContext(), "Валюта для конвертации не найдена", Toast.LENGTH_LONG).show();
                    }
                    double valuteValue = c.getValue();
                    resultOfConvertingTextView.setText(String.valueOf(enteredAmount/valuteValue));
                } else {
                    Toast.makeText(getApplicationContext(), "Формат валюты для конвертации неверен", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void refreshData() {
        MyViewModel model = ViewModelProviders.of(this).get(MyViewModel.class);
        model.loadData();
        currencies = model.getData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.currencies_recycler);
        CurrencyAdapter adapter = new CurrencyAdapter(currencies.getValue());
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    public Currency getCurrencyByValute(String valuteOfConverting, List<Currency> currencies) {
        Currency c = null;
        for (Currency item : currencies) {
            if (item.getCharCode().equals(valuteOfConverting)) {
                c = item;
            }
        }
        return c;
    }

    public boolean isCorrectValute(String valute) {
        if (valute.length() != 3) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }
}