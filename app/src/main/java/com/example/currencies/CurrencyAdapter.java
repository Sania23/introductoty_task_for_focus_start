package com.example.currencies;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {
    List<Currency> currencies;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(@NonNull CardView itemView) {
            super(itemView);
            cardView = itemView;
        }
    }

    public CurrencyAdapter(List<Currency> c) {
        currencies = c;
    }

    @NonNull
    @Override
    public CurrencyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_card, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyAdapter.ViewHolder holder, int position) {
        Currency c = currencies.get(position);
        CardView cardView = holder.cardView;
//        if (position % 2 != 0) {
//            cardView.setCardBackgroundColor(0xFFE6E6FA);
//        }
        TextView charCode = (TextView) cardView.findViewById(R.id.char_code);
        TextView name = (TextView) cardView.findViewById(R.id.name);
        TextView value = (TextView) cardView.findViewById(R.id.value);
        charCode.setText(c.getCharCode());
        name.setText(c.getName());
        value.setText(String.valueOf(c.getValue()));
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }
}
